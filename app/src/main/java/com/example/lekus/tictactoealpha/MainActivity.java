package com.example.lekus.tictactoealpha;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private int p1Score;
    private int p2Score;
    private TextView Player1_Text;
    private TextView Player2_Text;


    private int columnQuant = 3;
    private int linesQuant = 3;

    private int roundQuant;

    private Button[][] button = new Button[3][3];
    Random random = new Random();

    private boolean p1Turn = true;


    Button btnNewGame;
    View.OnClickListener ocbtnNewGame = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            newGame();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnNewGame = findViewById(R.id.btnNewGame);
        btnNewGame.setOnClickListener(ocbtnNewGame);
        Player1_Text = findViewById(R.id.Player1_Text);
        Player2_Text = findViewById(R.id.Player2_Text);

        fieldCreating();
    }


    @Override
    public void onClick(View v) {
        if (!((Button) v).getText().toString().equals("")) {
            return;
        }
        if (p1Turn == true) {
            ((Button) v).setText("X");
            ((Button) v).setTextColor(Color.RED);

        }
        roundQuant++;

        if (win1Check()) {
            if (p1Turn) {
                p1Win();
            }
        }


        if (roundQuant == 9) {
            draw();
        }

        if (p1Turn == false) {
            compTurn();
            roundQuant++;
        }

        if (win2Check()) {
            if (p1Turn == false) {
                p2Win();
            }
        }

        p1Turn = true;
    }

    public void compTurn() {

        String[][] field = new String[3][3];

        for (int i = 0; i < columnQuant; i++) {
            for (int j = 0; j < linesQuant; j++) {
                field[i][j] = button[i][j].getText().toString();
            }
        }

        int k = random.nextInt(3);
        int l = random.nextInt(3);
        if (field[k][l].equals("")) {
            button[k][l].setText("O");
            button[k][l].setTextColor(Color.BLUE);
        } else {
            compTurn();
        }
    }


    private void p1Win() {
        Toast.makeText(getApplicationContext(), "Player Tic WIN!", Toast.LENGTH_SHORT).show();
        p1Score++;
        updatepoints();
        newRound();

    }

    private void p2Win() {
        Toast.makeText(getApplicationContext(), "Player Tac WIN!", Toast.LENGTH_SHORT).show();
        p2Score++;
        updatepoints();
        newRound();
    }

    public void draw() {
        Toast.makeText(getApplicationContext(), "Draw!", Toast.LENGTH_SHORT).show();
        newRound();
    }

    public void fieldCreating() {
        for (int i = 0; i < columnQuant; i++) {
            for (int j = 0; j < linesQuant; j++) {
                String buttonID = "button_" + i + j;
                int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
                button[i][j] = findViewById(resID);
                button[i][j].setOnClickListener(this);
            }
        }
    }

    public boolean win1Check() {
        String[][] field = new String[3][3];

        for (int i = 0; i < columnQuant; i++) {
            for (int j = 0; j < linesQuant; j++) {
                field[i][j] = button[i][j].getText().toString();
            }
        }

        for (int i = 0; i < 3; i++) {
            if (field[i][0].equals(field[i][1]) && field[i][0].equals(field[i][2]) && !field[i][0].equals("")) {
                return true;
            }
        }

        for (int i = 0; i < 3; i++) {
            if (field[0][i].equals(field[1][i]) && field[0][i].equals(field[2][i]) && !field[0][i].equals("")) {
                return true;
            }
        }

        if (field[0][0].equals(field[1][1]) && field[0][0].equals(field[2][2]) && !field[0][0].equals("")) {
            return true;
        }

        if (field[0][2].equals(field[1][1]) && field[0][2].equals(field[2][0]) && !field[0][2].equals("")) {
            return true;
        }

        p1Turn = false;
        return false;
    }

    public boolean win2Check() {
        String[][] field = new String[3][3];

        for (int i = 0; i < columnQuant; i++) {
            for (int j = 0; j < linesQuant; j++) {
                field[i][j] = button[i][j].getText().toString();
            }
        }

        for (int i = 0; i < 3; i++) {
            if (field[i][0].equals(field[i][1]) && field[i][0].equals(field[i][2]) && !field[i][0].equals("")) {
                return true;
            }
        }

        for (int i = 0; i < 3; i++) {
            if (field[0][i].equals(field[1][i]) && field[0][i].equals(field[2][i]) && !field[0][i].equals("")) {
                return true;
            }
        }

        if (field[0][0].equals(field[1][1]) && field[0][0].equals(field[2][2]) && !field[0][0].equals("")) {
            return true;
        }

        if (field[0][2].equals(field[1][1]) && field[0][2].equals(field[2][0]) && !field[0][2].equals("")) {
            return true;
        }

        p1Turn = true;

        return false;
    }

    public void newGame() {

        for (int i = 0; i < columnQuant; i++) {
            for (int j = 0; j < linesQuant; j++) {
                button[i][j].setText("");
            }
        }

        roundQuant = 0;
        p1Turn = true;

        p1Score = 0;
        p2Score = 0;

        Player1_Text.setText("Player Tic: 0");
        Player1_Text.setTextColor(Color.RED);
        Player2_Text.setText("Player Tac: 0");
        Player2_Text.setTextColor(Color.BLUE);

    }

    public void newRound() {
        for (int i = 0; i < columnQuant; i++) {
            for (int j = 0; j < linesQuant; j++) {
                button[i][j].setText("");
            }
        }
        roundQuant = 0;
        p1Turn = true;
    }


    public void updatepoints() {
        Player1_Text.setText("Player Tic: " + p1Score);
        Player1_Text.setTextColor(Color.RED);
        Player2_Text.setText("Player Tac: " + p2Score);
        Player2_Text.setTextColor(Color.BLUE);

    }


}
